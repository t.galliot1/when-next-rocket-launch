@extends('layout' )

@section('content')
    <div class="reveal">
        <!--<div class="borders">-->
        <!--<div class="top"></div>-->
        <!--<div class="right"></div>-->
        <!--<div class="bottom"></div>-->
        <!--<div class="left"></div>-->
        <!--</div>-->
        <header>
            <div class="logo-wrapper">
                <img src="lib/images/sal_logo_white_sm.png">
            </div>
            <nav>
                <a href="#/1">Next mission</a>&nbsp;&nbsp;
                <a href="/timeline">Missions timeline</a>
            </nav>
            <!--<div class="search-wrap">-->
            <!--<form action="" autocomplete="on">-->
            <!--<input id="search" name="search" type="text" placeholder="Search for previous missions">-->
            <!--<input id="search_submit" value="Search" type="submit">-->
            <!--</form>-->
            <!--</div>-->
        </header>
        <div class="slides">
            <!-- Countdown -->
            <section data-transition="slide"
                     data-background-image="lib/images/launch2.jpg"
                     data-background-size="cover">
                <div class="wrapper">
                    <h2>Next mission in</h2>
                    <div class="countdown">
                        <ul>
                            <li><span id="days"></span>days</li>
                            <li><span id="hours"></span>Hours</li>
                            <li><span id="minutes"></span>Minutes</li>
                            <li><span id="seconds"></span>Seconds</li>
                        </ul>
                    </div>
                    <p>Location</p>
                    <p><small>Sub-informations</small></p>
                    <a href="#" class="resume-button">
                        Missions statistiques
                    </a>
                </div>
            </section>
            <!-- Stats -->
            <section data-transition="slide">
                <div class="wrapper align-left">
                    <h2>NASA missions</h2>
                    <div class="grid">
                        <div class="col6 card">
                            <div class="card-body">
                                <h5 class="card-title">Vestibulum est arcu</h5>
                                <p class="card-text">Nam et risus a nulla sagittis laoreet. Sed ultrices lectus velit, et venenatis urna dapibus ac.</p>
                                <a href="#" class="card-link">View details</a>
                            </div>
                        </div>
                        <div class="col6 card">
                            <div class="card-body">
                                <h5 class="card-title">Vestibulum est arcu</h5>
                                <p class="card-text">Nam et risus a nulla sagittis laoreet. Sed ultrices lectus velit, et venenatis urna dapibus ac.</p>
                                <a href="#" class="card-link">View details</a>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="navigate-down resume-button">
                        Missions timeline
                    </a>
                </div>
            </section>
            <!-- Stats -->
            <section data-transition="slide">
                <div class="wrapper align-left">
                    <h2>NASA missions</h2>
                    <div class="col12 card">
                        <div class="card-body">
                            <h5 class="card-title">Vestibulum est arcu</h5>
                            <p class="card-text">Nam et risus a nulla sagittis laoreet. Sed ultrices lectus velit, et venenatis urna dapibus ac.</p>
                            <a href="#" class="card-link">View details</a>
                        </div>
                    </div>
                    <div class="col12 card">
                        <div class="card-body">
                            <h5 class="card-title">Vestibulum est arcu</h5>
                            <p class="card-text">Nam et risus a nulla sagittis laoreet. Sed ultrices lectus velit, et venenatis urna dapibus ac.</p>
                            <a href="#" class="card-link">View details</a>
                        </div>
                    </div>
                    <a href="#" class="navigate-down resume-button">
                        Missions timeline
                    </a>
                </div>
            </section>
            <section>
                <!-- Timeline -->
                <section data-transition="slide"
                         data-background-image="lib/images/launch3.jpg"
                         data-background-size="cover">
                    <div class="wrapper">
                        <div class="grid timeline">
                            <div class="col12 timeline-meta">
                                <p>19/10/2018</p>
                                <h3>Previous mission 1</h3>
                            </div>
                            <div class="col6 card">
                                <div class="card-body">
                                    <h5 class="card-title">Vestibulum est arcu</h5>
                                    <p class="card-text">Nam et risus a nulla sagittis laoreet. Sed ultrices lectus velit, et venenatis urna dapibus ac.</p>
                                    <a href="#" class="card-link">View mission details</a>
                                </div>
                            </div>
                            <div class="col6 card">
                                <div class="card-body">
                                    <h5 class="card-title">Stats</h5>
                                    <table class="card-table">
                                        <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Value</th>
                                            <th>Quantity</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Apples</td>
                                            <td>$1</td>
                                            <td>7</td>
                                        </tr>
                                        <tr>
                                            <td>Lemonade</td>
                                            <td>$2</td>
                                            <td>18</td>
                                        </tr>
                                        <tr>
                                            <td>Bread</td>
                                            <td>$3</td>
                                            <td>2</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="navigate-down resume-button">
                            Previous mission
                        </a>
                    </div>
                </section>
                <section data-transition="slide"
                         data-background-image="lib/images/launch4.jpg"
                         data-background-size="cover">
                    <div class="wrapper">
                        <div class="grid timeline">
                            <div class="col12 timeline-meta">
                                <p>10/09/2018</p>
                                <h3>Previous mission 2</h3>
                            </div>
                            <div class="col6 card">
                                <div class="card-body">
                                    <h5 class="card-title">Vestibulum est arcu</h5>
                                    <p class="card-text">Nam et risus a nulla sagittis laoreet. Sed ultrices lectus velit, et venenatis urna dapibus ac.</p>
                                    <a href="#" class="card-link">View mission details</a>
                                </div>
                            </div>
                            <div class="col6 card">
                                <div class="card-body">
                                    <h5 class="card-title">Stats</h5>
                                    <table class="card-table">
                                        <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Value</th>
                                            <th>Quantity</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Apples</td>
                                            <td>$1</td>
                                            <td>7</td>
                                        </tr>
                                        <tr>
                                            <td>Lemonade</td>
                                            <td>$2</td>
                                            <td>18</td>
                                        </tr>
                                        <tr>
                                            <td>Bread</td>
                                            <td>$3</td>
                                            <td>2</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="navigate-down resume-button">
                            Previous mission
                        </a>
                    </div>
                </section>
                <section data-transition="slide"
                         data-background-image="lib/images/launch5.jpg"
                         data-background-size="cover">
                    <div class="wrapper">
                        <div class="grid timeline">
                            <div class="col12 timeline-meta">
                                <p>09/08/2018</p>
                                <h3>Previous mission 3</h3>
                            </div>
                            <div class="col6 card">
                                <div class="card-body">
                                    <h5 class="card-title">Vestibulum est arcu</h5>
                                    <p class="card-text">Nam et risus a nulla sagittis laoreet. Sed ultrices lectus velit, et venenatis urna dapibus ac.</p>
                                    <a href="#" class="card-link">View mission details</a>
                                </div>
                            </div>
                            <div class="col6 card">
                                <div class="card-body">
                                    <h5 class="card-title">Stats</h5>
                                    <table class="card-table">
                                        <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Value</th>
                                            <th>Quantity</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Apples</td>
                                            <td>$1</td>
                                            <td>7</td>
                                        </tr>
                                        <tr>
                                            <td>Lemonade</td>
                                            <td>$2</td>
                                            <td>18</td>
                                        </tr>
                                        <tr>
                                            <td>Bread</td>
                                            <td>$3</td>
                                            <td>2</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <a href="#/1" class="resume-button">
                            Back to next mission
                        </a>
                    </div>
                </section>
            </section>
        </div>
    </div>




    <script src="lib/js/head.min.js"></script>
    <script src="lib/js/countdown.js"></script>
    <script src="js/reveal.js"></script>

    <script>
        // More info about config & dependencies:
        // - https://github.com/hakimel/reveal.js#configuration
        // - https://github.com/hakimel/reveal.js#dependencies
        Reveal.initialize({
            dependencies: [
                { src: 'plugin/markdown/marked.js' },
                { src: 'plugin/markdown/markdown.js' },
                { src: 'plugin/notes/notes.js', async: true },
                { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
            ]
        });
    </script>
@endsection