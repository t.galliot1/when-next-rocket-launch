<?php

namespace Tests\Feature;

use App\Locationbase;
use League\Csv\Reader;
use League\Geotools\Coordinate\Coordinate;
use Tests\TestCase;

class ImportLocationbase extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testImportLocationBase()
    {
        $csvFile = storage_path('app'.DIRECTORY_SEPARATOR.'scraps').DIRECTORY_SEPARATOR.'locationbase.csv';

        //load the CSV document from a file path
        $csv = Reader::createFromPath($csvFile, 'r');
        $csv->setHeaderOffset(0);
        $csv->setOutputBOM(Reader::BOM_UTF8);

        $header = $csv->getHeader(); //returns the CSV header record
        $records = $csv->getRecords(); //returns all the CSV records as an Iterator object

        foreach($records as $i => $record) {
            $site = new Locationbase();

            $site->country = $record["Pays"];
            $site->name = $record["Lieu"];
            $site->operational = $record["Opérationnel de/à"];
            $site->coordinates = $record["Coordonnées"];
            try {
                $coordinates  = new Coordinate(str_replace('O', 'W', $record["Coordonnées"]));
                $site->lat = $coordinates->getLatitude();
                $site->lng = $coordinates->getLongitude();
            }
            catch(\Exception $e) {

                $site->lat = "";
                $site->lng = "";
            }
            $count  = str_replace(' ', '', trim($record["Nombre de fusées lancées"], '>'));
            $site->launchCount = $count ?: 0;
            $site->altitude = $record["Altitude la plus élevée atteinte"];
            $site->notes = $record["Notes"];

            $site->save();
        }
    }

}
