<?php

namespace Tests\Feature;

use App\Mission;
use App\Payload;
use App\Rocket;
use App\Site;
use League\Csv\Reader;
use Tests\TestCase;

class ImportSkyRocket extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testImportSkyRocketDe()
    {
        $csvFile = storage_path('app'.DIRECTORY_SEPARATOR.'scraps').DIRECTORY_SEPARATOR.'skyrocketde.csv';

        //load the CSV document from a file path
        $csv = Reader::createFromPath($csvFile, 'r');
        $csv->setHeaderOffset(0);

        $header = $csv->getHeader(); //returns the CSV header record
        $records = $csv->getRecords(); //returns all the CSV records as an Iterator object

        foreach($records as $i => $record) {

            if($record["Date"] == "null") {
                continue;
            }

            $isNotLaunched = strpos($record["Date"], 'x') !== false || strpos($record["Date"], '?') !== false;
            $hasFailed = strpos(strtolower($record["Remark"]), "fail") !== false;

            $payloads = array_filter(explode("\n", str_replace("\t", "", $record["Payload(s)"])));

            $rocket = new Rocket();
            $rocket->name = $record["Launch Vehicle"];
            $rocket->save();

            $site = new Site();
            $site->name = $record["Site"];
            $site->save();

            $mission = new Mission();
            $mission->name = $record["ID"];
            $mission->status =  $hasFailed ? 'failed' : $isNotLaunched ? 'scheduled' : "launched";
            if(!$isNotLaunched) {
                $mission->launching_at = implode('-', array_reverse(explode('.', $record["Date"])));
            }
            $mission->rocket_id = $rocket->id;
            $mission->site_id = $site->id;
            $mission->remarks = $record["Remark"];
            $mission->save();

            foreach($payloads as $payloadName) {
                $payload = new Payload();
                $payload->name = $payloadName;
                $payload->mission_id = $mission->id;
                $payload->save();
            }
        }
    }
}
