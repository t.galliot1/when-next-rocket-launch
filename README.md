# When Next Rocket Launch

Space Apps 2018 project

# Setup

- `git clone https://gitlab.com/simplon-roanne/when-next-rocket-launch`
- `composer install`
- Copier .env.example vers .env et ajuster les valeurs
- `php artisan migrate`