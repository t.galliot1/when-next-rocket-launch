<?php

namespace App\Http\Controllers;

use App\Mission;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function timeline()
    {
        $missions = Mission::with('site', 'rocket', 'payloads')->get();

        return view('timeline', compact('missions'));
    }
}
